import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MediaDeviceService } from '../shared/services/media-device.service';
import { Router } from '@angular/router';
import { RoutesEnum } from '../shared/enums/routes.enum';

@Component({
  selector: 'app-meeting-room',
  templateUrl: './meeting-room.component.html',
  styleUrls: ['./meeting-room.component.css']
})
export class MeetingRoomComponent implements OnInit, AfterViewInit {
  @ViewChild('video', null) video: any;

  public _video;

  ngAfterViewInit(): void {
    this._video = this.video.nativeElement;
    try{
      this._video.srcObject = this.mediaDeviceService.getUserMedia().clone();
      this._video.play();
    } catch (err) {
      this.router.navigate(['/'+RoutesEnum.ROUTE_ASK_AUTHORIZATION]);
    }
  }

  constructor(private mediaDeviceService: MediaDeviceService, private router: Router) { }

  ngOnInit() { }

  public muteUnmute() {
    this._video.muted = !this._video.muted;
  }

  public onOffCam() {
    if (this._video.srcObject.getVideoTracks()[0].readyState === "ended") {
      this._video.srcObject = this.mediaDeviceService.getUserMedia().clone();
      this._video.play();
    } else {
      this._video.srcObject.getVideoTracks()[0].stop();
    }
  }

  public isCameraOn() {
    return this._video && this._video.srcObject ? this._video.srcObject.getVideoTracks()[0].readyState !== "ended" : true;
  }

}
