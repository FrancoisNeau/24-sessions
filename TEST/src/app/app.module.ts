import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { appRoutes } from './app.routing';
import { AskAuthorizationsComponent } from './ask-authorizations/ask-authorizations.component';
import { ChooseDevicesComponent } from './choose-devices/choose-devices.component';
import { MeetingRoomComponent } from './meeting-room/meeting-room.component';

@NgModule({
  declarations: [
    AppComponent,
    AskAuthorizationsComponent,
    ChooseDevicesComponent,
    MeetingRoomComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
    MatSelectModule,
    BrowserAnimationsModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
