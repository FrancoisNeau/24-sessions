import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AskAuthorizationsComponent } from './ask-authorizations.component';

describe('AskAuthorizationsComponent', () => {
  let component: AskAuthorizationsComponent;
  let fixture: ComponentFixture<AskAuthorizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AskAuthorizationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AskAuthorizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
