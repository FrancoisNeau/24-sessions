import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MediaDeviceService } from '../shared/services/media-device.service';
import { RoutesEnum } from '../shared/enums/routes.enum';

@Component({
  selector: 'app-ask-authorizations',
  templateUrl: './ask-authorizations.component.html',
  styleUrls: ['./ask-authorizations.component.css']
})
export class AskAuthorizationsComponent implements OnInit {

  public isRefusedMessageDisplayed: boolean = false;

  constructor(private router: Router, private mediaDeviceService: MediaDeviceService) { }

  ngOnInit() {
  }

  public agreeAccess() {
    navigator.mediaDevices.getUserMedia({audio: true, video: true})
      .then(res => {
        this.mediaDeviceService.setUserMedia(res);
        this.router.navigate(['/'+RoutesEnum.ROUTE_CHOOSE_DEVICES]);
      }).catch(error => {
        this.refuseAccess();
      });
  }

  public refuseAccess() {
    this.isRefusedMessageDisplayed = true;
  }

}
