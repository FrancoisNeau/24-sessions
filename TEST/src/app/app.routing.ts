import { AppComponent } from './app.component';
import { ChooseDevicesComponent } from './choose-devices/choose-devices.component';
import { AskAuthorizationsComponent } from './ask-authorizations/ask-authorizations.component';
import { MeetingRoomComponent } from './meeting-room/meeting-room.component';
import { RoutesEnum } from './shared/enums/routes.enum';

export const appRoutes = [
    { path: '', redirectTo: 'ask-authorizations', pathMatch: 'full'},
    { path: RoutesEnum.ROUTE_CHOOSE_DEVICES, component: ChooseDevicesComponent},
    { path: RoutesEnum.ROUTE_ASK_AUTHORIZATION, component: AskAuthorizationsComponent},
    { path: RoutesEnum.ROUTE_MEETING_ROOM, component: MeetingRoomComponent}
];