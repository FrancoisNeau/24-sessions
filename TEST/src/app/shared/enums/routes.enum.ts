export enum RoutesEnum {
    "ROUTE_ASK_AUTHORIZATION" = "ask-authorizations",
    "ROUTE_CHOOSE_DEVICES" = "choose-devices",
    "ROUTE_MEETING_ROOM" = "meeting-room"
}