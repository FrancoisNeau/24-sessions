import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MediaDeviceService {

  private userMedia: MediaStream;

  constructor() { }

  public setUserMedia(userMedia: MediaStream) {
    this.userMedia = userMedia;
  }

  public getUserMedia(): MediaStream {
    return this.userMedia;
  }
}
