import { TestBed } from '@angular/core/testing';

import { MediaDeviceService } from './media-device.service';

describe('MediaDeviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MediaDeviceService = TestBed.get(MediaDeviceService);
    expect(service).toBeTruthy();
  });
});
