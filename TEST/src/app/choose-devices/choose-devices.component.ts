import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MediaDeviceService } from '../shared/services/media-device.service';
import { Router } from '@angular/router';
import { RoutesEnum } from '../shared/enums/routes.enum';

@Component({
  selector: 'app-choose-devices',
  templateUrl: './choose-devices.component.html',
  styleUrls: ['./choose-devices.component.css']
})
export class ChooseDevicesComponent implements OnInit, AfterViewInit {
  @ViewChild('video', null) video: any;
  
  public audioSelected: InputDeviceInfo;
  public videoSelected: InputDeviceInfo;
  public audioOutputSelected: MediaDeviceInfo;

  public audioSources = [];
  public videoSources = [];
  public audioOutputs = [];

  public _video;

  ngAfterViewInit(): void {
    this._video = this.video.nativeElement;
    this._video.srcObject = this.mediaDeviceService.getUserMedia();
    this._video.play();
    console.log(this._video.srcObject);

    navigator.mediaDevices.enumerateDevices().then(res => {
      res.forEach(source => {
        if (source.kind === 'audioinput') {
          this.audioSources.push(source);
        } else if (source.kind === 'videoinput') {
          this.videoSources.push(source);
        } else if (source.kind === 'audiooutput') {
          this.audioOutputs.push(source);
        }
      });
      this.audioSelected = this.audioSources[0];
      this.videoSelected = this.videoSources[0];
      this.audioOutputSelected = this.audioOutputs[0];
    });
  }

  constructor(private mediaDeviceService: MediaDeviceService, private router: Router) { }

  ngOnInit() {
  }

  public mediaChanged(kind) {
    if (kind === 'audioOutput') {
      this._video.setSinkId(this.audioOutputSelected.deviceId).then(res => {
        console.log('audioOutput changed');
      });
    } else {
      if (this._video.srcObject) {
        this._video.srcObject.getTracks().forEach(track => {
          track.stop();
        });
      }
      const audioSource = this.audioSelected ? this.audioSelected.deviceId : undefined;
      const videoSource = this.videoSelected ? this.videoSelected.deviceId : undefined;
      const constraints = {
        audio: {deviceId: audioSource ? {exact: audioSource} : undefined},
        video: {deviceId: videoSource ? {exact: videoSource} : undefined}
      };
      navigator.mediaDevices.getUserMedia(constraints).then(res => {
        this._video.srcObject = res;
        this.mediaDeviceService.setUserMedia(res);
      });
    }
  }

  public redirectToMeetingRoom() {
    this.router.navigate(['/'+RoutesEnum.ROUTE_MEETING_ROOM]);
  }

}
