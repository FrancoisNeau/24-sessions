import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseDevicesComponent } from './choose-devices.component';

describe('ChooseDevicesComponent', () => {
  let component: ChooseDevicesComponent;
  let fixture: ComponentFixture<ChooseDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
